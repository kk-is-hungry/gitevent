$(function() {
    const { layer, form } = layui
    let index = null
    let add = {
        act: 'add',
        advimgpos: '',
        advimgdesc: '',
        advimglink: '',
        advimgsrc: ''
    }
    let data = {
        act: 'getlist',
        keys: '',
        posid: '',
        start: 1,
        pagesize: 2
    }
    let select = {
            act: 'getlist',
        }
        //列表与添加切换
    $('.toggle button').click(function() {
        $(this).addClass('active').siblings().removeClass('active');
        let index = $(this).index()
        $('.layui-card').eq(index).addClass('current').siblings().removeClass('current')
    });
    getList()
        //获取广告图列表
    function getList() {
        $.get('http://122.51.249.55:8083/api/advimg', data, res => {
            console.log('广告图');
            console.log(res);
            if (res.code !== 200) return layer.msg(res.msg)
                //模板引擎
            let tbodyHTML = template('tpl', res)
            $('tbody').html(tbodyHTML)
            let selectHTML = template('tpl-cate', res)
            $('#select').html(selectHTML)


            //渲染分页器
            renderPage(res.allNum)
            form.render();

        })
    }
    //下拉选择框
    getDownList()

    function getDownList() {
        $.get('http://122.51.249.55:8083/api/advpos', select, res => {
            console.log('广告位');
            console.log(res);
            let queryHTML = template('tpl-query', res)
            $('#query').html(queryHTML)
            form.render();
            var viewer = new Viewer(document.getElementById('jq22'));
        })
    }
    //分页器
    function renderPage(total) {
        layui.use('laypage', function() {
            var laypage = layui.laypage;

            //执行一个laypage实例
            laypage.render({
                elem: 'test2', //注意，这里的 test1 是 ID，不用加 # 号
                count: total, //数据总数，从服务端得到
                limit: data.pagesize,
                limits: [2, 3, 5],
                curr: data.start,
                layout: ['count', 'limit', 'prev', 'page', 'next'],
                jump: function(obj, first) {
                    //obj包含了当前分页的所有参数，比如：
                    data.start = obj.curr,
                        data.pagesize = obj.limit
                        //首次不执行
                    if (!first) {
                        //do something
                        getList()
                    }
                }
            });
        });
    }
    //删除
    $('tbody').on('click', '.btn-delete', function() {
            var id = $(this).attr('data-id')
                // 询问用户是否要删除数据
            let query = {
                id,
                act: 'del'
            }
            layer.confirm('确定删除该广告图吗?', { icon: 3, title: '提示' }, function(index) {
                $.get('http://122.51.249.55:8083/api/advimg', query, res => {
                    console.log(res);
                    if (res.code !== 200) return layer.msg(res.msg)
                    layer.msg(res.msg)
                    console.log($('.btn-delete').length);
                    if ($('.btn-delete').length == 1 && data.start !== 1) {
                        data.start--;
                    }
                    getList()
                })
                layer.close(index);
            });
        })
        // 上传图片
    layui.use('upload', function() {
        var upload = layui.upload;
        //执行实例
        var uploadInst = upload.render({
            elem: '#test1', //绑定元素
            url: 'http://122.51.249.55:8083/api/upload', //上传接口
            field: 'img',
            done: function(res) {
                //上传完毕回调
                console.log(res);
                $('#image').attr('src', res.data[0].url)
                add.advimgsrc = res.data[0].path
                console.log(add);
            },
            error: function() {
                //请求异常回调
            }
        });
    });
    //提交新增表单
    $('#add-form').submit(function(e) {

    });
    //编辑button
    $(document).on('click', '.edit-btn', function() {
            let data = {
                    id: $(this).attr('data-id'),
                    act: 'edit',
                    advimgname: $(this).attr('data-pos'),
                    advimgdesc: $(this).attr('data-desc'),
                    advimglink: $(this).attr('data-link'),
                    davimgsrc: 'http://122.51.249.55:8083' + $(this).attr('data-src'),
                    advimgpos: $(this).attr('data-pos')
                }
                // form.val('editForm',data) 

            index = layer.open({
                type: 1,
                title: '修改广告图信息',
                area: ['500px', '420px'],
                content: $('#dialog-add').html()
            });
            form.val("editForm", data)
            $('#addimg').attr('src', data.davimgsrc)
        })
        //关闭弹出层
    $(document).on('click', '.cancel', function() {
            layer.close(index)
        })
        //查询
    $('.query-form').submit(function(e) {
            e.preventDefault();
            let query = form.val("query")
            data = {...data, ...query };
            console.log(data);
            getList()
        })
        //重置
    $('#resetting').click(function() {
            data = {
                act: 'getlist',
                keys: '',
                posid: '',
                start: 1,
                pagesize: 2
            }
            getList()
        })
        // 添加
    $('#add-form').submit(function(e) {
            e.preventDefault()
                // console.log(add);
            let data = form.val("addForm")
            data = {...add, ...data }
            delete data.img
            console.log(data);

            $.post('http://122.51.249.55:8083/api/advimg', data, res => {
                if (res.code !== 200) return layer.msg(res.msg)
                layer.msg(res.msg)
                $('.quxiao').click()
                $('.list').click()
                getList()
            })
        })
        //修改
    $(document).on('submit', '#edit-form', function(e) {
        e.preventDefault()
        let imgsrc = $('#addimg').attr('src').split('http://122.51.249.55:8083/')[1]
        let data = $(this).serialize() + '&advimgsrc=' + imgsrc

        console.log(data);
        $.post('http://122.51.249.55:8083/api/advimg', data, res => {
            console.log(res);
            if (res.code !== 200) return layer.msg(res.msg)
            layer.msg(res.msg)
            layer.close(index)
            getList()
        })
    })
})