$(function() {
    var layer = layui.layer
    var form = layui.form


    // 定义美化时间的过滤器
    template.defaults.imports.dataFormat = function(date) {
        const dt = new Date(parseInt(date))

        var y = dt.getFullYear()
        var m = padZero(dt.getMonth() + 1)
        var d = padZero(dt.getDate())

        var hh = padZero(dt.getHours())
        var mm = padZero(dt.getMinutes())
        var ss = padZero(dt.getSeconds())

        return y + '-' + m + '-' + d + ' ' + hh + ':' + mm + ':' + ss
    }

    // 定义补零的函数
    function padZero(n) {
        return n > 9 ? n : '0' + n
    }

    var q = {
        act: 'getlist',
        id: '',
        cateid: '',
        keywords: '',
        time: '',
        start: '1',
        pagesize: '10'
    }
    initTable()
    initCate()
        //获取文章列表
    function initTable() {
        $.ajax({
            type: "get",
            url: "http://122.51.249.55:8083/api/article",
            data: q,
            success: function(res) {
                // console.log(res)
                if (res.code !== 200) {
                    return layer.msg('获取文章列表失败！')
                }


                console.log(res);
                // localStorage.setItem('title', res.data[0].title)
                // localStorage.setItem('catename', res.data[0].catename)

                // localStorage.setItem('author', res.data[0].author)
                // localStorage.setItem('num', res.data[0].click)
                // localStorage.setItem('content', res.data[0].content)
                // localStorage.setItem('keywords', res.data[0].keywords)
                // localStorage.setItem('description', res.data[0].description)

                //使用模板引擎
                var htmlStr = template('tpl-table', res)

                $('tbody').html(htmlStr)
                    //调用渲染分页
                renderPage(res.allNum)
            }
        });
    }




    //初始化文章分类
    function initCate() {
        $.ajax({
            type: "get",
            url: "http://122.51.249.55:8083/api/article",
            data: q,
            success: function(res) {
                if (res.code !== 200) {
                    return layer.msg('获取分类列表失败！')
                }
                //使用模板引擎
                var htmlStr = template('tpl-cate', res)
                    // console.log(htmlStr);
                $('[name=cate_id]').html(htmlStr)
                    //通过layui重新渲染
                var form = layui.form
                form.render()
            }
        });
    }

    //为筛选表单绑定submit
    $('#form-search').on('submit', function(e) {
        e.preventDefault()
            // 获取表单中选中项的值
        var cate_id = $('[name=cate_id]').val()
        var keywords = $('#keywords').val()
            // var startDate = $('#startDate').val()
            // 为查询参数对象 q 中对应的属性赋值
        q.cateid = cate_id
        q.keywords = keywords
            // q.time = startDate

        // 根据最新的筛选条件，重新渲染表格的数据
        initTable()
    })

    $(function() {
        $('#btnReset').click(function() {
            document.getElementById("form-search").reset()
        })
    })



    // 定义渲染分页的方法
    layui.use('laypage', function() {});

    function renderPage(allNum) {
        var laypage = layui.laypage;
        // 调用 laypage.render() 方法来渲染分页的结构
        laypage.render({
            elem: 'pageBox', // 分页容器的 Id
            count: allNum, //数据总数，从服务端得到
            limit: q.pagesize,
            curr: q.start,
            layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'], //自定义排版
            limits: [2, 3, 5, 10],
            // 分页发生切换的时候，触发 jump 回调

            jump: function(obj, first) {

                q.start = obj.curr
                    // 把最新的条目数，赋值到 q 这个查询参数对象的 pagesize 属性中
                q.pagesize = obj.limit
                    // 根据最新的 q 获取对应的数据列表，并渲染表格
                    // initTable()
                if (!first) {
                    initTable()
                }
            }
        })
    }

    //删除功能
    $('tbody').on('click', '.btn-delete', function() {
        // 获取到文章的 id
        // console.log(2222);
        var layer = layui.layer
        let data = {
            id: $(this).attr('data-id'),
            act: "del"
        }
        layer.confirm('确认删除?', { icon: 3, title: '提示' }, function(index) {
            $.ajax({
                method: 'get',
                url: "http://122.51.249.55:8083/api/article",
                data,
                success: function(res) {
                    if (res.code !== 200) {
                        return layer.msg('删除文章失败！')
                    }
                    layer.msg('删除文章成功！')
                        //当数据删除完成后，需要判断当前页面是否有内容没有则页值-1
                        // if (len === 1) {
                        //len为1说明删除完毕 页面没有内容
                        // q.pagenum = q.pagenum === 1 ? 1 : q.pagenum - 1
                        // }
                    initTable()
                }
            })
            layer.close(index);
        })
    })
    $("tbody").on("click", "#edit-box", function() {

        // location.href = "../article.html"
        localStorage.setItem("articleid", $(this).attr("data-id"))
    })
})