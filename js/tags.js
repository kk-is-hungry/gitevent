$(function () {
    $('strong').on('click', function () {
        $(this).css({
            'background-color': '#fb6d49',
            'color': "#fff"
        })
        $('b').css({
            'background-color': '#fff',
            'color': "black"
        })
    })
    $('b').on('click', function () {
        $(this).css({
            'background-color': '#fb6d49',
            'color': "#fff"
        })
        $('strong').css({
            'background-color': '#fff',
            'color': "black"
        })
    })
    $('strong').on('mouseover', function () {
        if ($(this).css('color') === 'rgb(0, 0, 0)') {
            $(this).css('color', '#fb6d49')
        }
    })
    $('strong').on('mouseout', function () {
        if ($(this).css('color') === 'rgb(251, 109, 73)') {
            $(this).css('color', 'black')
        }
    })
    $('b').on('mouseover', function () {
        if ($(this).css('color') === 'rgb(0, 0, 0)') {
            $(this).css('color', '#fb6d49')
        }
    })
    $('b').on('mouseout', function () {
        if ($(this).css('color') === 'rgb(251, 109, 73)') {
            $(this).css('color', 'black')
        }
    })

    var laypage = layui.laypage;
    var data = {
        act: "getlist",
        start: 1,
        pagesize: 10
    }
    // 获取分类列表
    getTagList()
    var id;
    // 定义获取标签列表函数
    function getTagList() {
        $.ajax({
            type: "get",
            url: "http://122.51.249.55:8083/api/tag",
            data: data,
            success: function (res) {
                if (res.code !== 200) {
                    return layui.layer.msg(res.msg)
                }
                // console.log(res);
                var htmlStr = template('tag_table', res)
                $('tbody').html(htmlStr)

                renderPage(res.allNum)
                // console.log(res.allNum);
            }

        });
    }
    // 通过代理的方式，给tag_edit绑定click事件
    $('tbody').on('click', '.tag_edit', function () {
        layer.open({
            type: 1,
            title: ['修改标签信息', 'font-size:20px;'],
            area: ['550px', '300px'],
            // scrollbar: false,
            content: $('#tpl_tag').html()
        });

        id = $(this).attr('data-id')
        catename = $(this).attr('data-catename')

        $('#input-edit').attr('placeholder', catename)
    })
    // 通过代理的形式，为修改标签的表单绑定 submit 事件
    $('body').on('submit', '.tag-edit', function (e) {
        // 阻止默认提交
        e.preventDefault()
        var data1 = $('.tag-edit').serialize().split('=')[1]

        data1 = decodeURIComponent(data1, true);
        $.ajax({
            type: "post",
            url: "http://122.51.249.55:8083/api/tag",
            data: {
                id: id,
                act: 'edit',
                tagname: data1,
            },
            success: function (res) {
                if (res.code !== 200) {
                    return layui.layer.msg(res.msg)
                }
                layui.layer.msg(res.msg)
                layui.layer.closeAll()
                getTagList()

            }
        });
    })
    // 通过代理的形式，为删除按钮绑定点击事件
    $('body').on('click', '.cate-delete', function () {
        // 获取删除按钮的个数
        var len = $('.cate-delete').length
        var id1 = $(this).attr('data-id1');
        console.log(id1);
        // 提示用户是否要删除
        layer.confirm('确认删除?', { icon: 3, title: '提示' }, function () {
            $.ajax({
                method: 'GET',
                url: 'http://122.51.249.55:8083/api/tag',
                data: {
                    id: id1,
                    act: 'del'
                },
                success: function (res) {
                    if (res.code !== 200) {
                        return layui.layer.msg(res.msg)
                    }
                    layui.layer.msg(res.msg)
                    if (len === 1) {
                        // 如果 len 的值等于1，证明删除完毕之后，页面上就没有任何数据了
                        // 页码值最小必须是 1
                        data.pagesize = data.pagesize === 1 ? 1 : data.pagesize - 1
                    }
                    getTagList()


                }
            })
        })
    })


    // 通过代理的形式，为添加标签的表单绑定 submit 事件
    $('body').on('submit', '.form-add', function (e) {
        // 阻止默认提交
        e.preventDefault()
        var data2 = $('.form-add').serialize().split('=')[1]

        data2 = decodeURIComponent(data2, true);
        $.ajax({
            type: "post",
            url: "http://122.51.249.55:8083/api/tag",
            data: {

                act: 'add',
                tagname: data2,
            },
            success: function (res) {
                if (res.code !== 200) {
                    return layui.layer.msg(res.msg)
                }
                layui.layer.msg(res.msg)
                $('.form-add')[0].reset()
                getTagList()

            }
        });
    })
    // 定义渲染分页的方法
    function renderPage(allNum) {
        // console.log(allNum);
        var laypage = layui.laypage;

        laypage.render({
            elem: 'pageBox', // 分页容器的 Id
            count: allNum, // 总数据条数
            limit: data.pagesize, // 每页显示几条数据
            curr: data.start, // 设置默认被选中的分页
            layout: ['count', 'limit', 'prev', 'page', 'next'],
            limits: [2, 3, 5, 10],
            theme: '#fb6d49',
            // 分页发生切换的时候，触发 jump 回调
            // 触发 jump 回调的方式有两种：
            // 1. 点击页码的时候，会触发 jump 回调
            // 2. 只要调用了 laypage.render() 方法，就会触发 jump 回调
            jump: function (obj, first) {
                // 可以通过 first 的值，来判断是通过哪种方式，触发的 jump 回调
                // 如果 first 的值为 true，证明是方式2触发的
                // 否则就是方式1触发的
                console.log(first)
                console.log(obj.curr)
                // 把最新的页码值，赋值到 q 这个查询参数对象中
                data.start = obj.curr
                // 把最新的条目数，赋值到 q 这个查询参数对象的 pagesize 属性中
                data.pagesize = obj.limit
                // 根据最新的 q 获取对应的数据列表，并渲染表格
                // initTable()
                if (!first) {
                    getTagList()
                }
            }
        })

    }
})