$(function() {
    var layer = layui.layer
    var laypage = layui.laypage
    var form = layui.form
        // 自定义校验规则
    form.verify({
        pwd: [/^\d+\,\d+$/, '广告图尺寸，宽高之间注意英文逗号间隔']
    })
    var q = {
        act: 'getlist',
        id: 'id',
        cateid: '',
        keywords: '',
        time: '',
        start: '1',
        pagesize: '10'
    }
    initTable()

    //122.51.249.55:8083/api/article?act=getlist&pagesize=10&start=1&keywords=&ipttime=&time=&cateid=
    //获取广告位接口
    function initTable() {
        $.ajax({
            type: "get",
            url: "http://122.51.249.55:8083/api/advpos",
            data: q,
            success: function(res) {
                if (res.code !== 200) {
                    return layer.msg('获取广告列表失败！')
                    console.log(res);
                }
                //使用模板引擎
                var htmlStr = template('tpl-table', res)
                $('tbody').html(htmlStr)
                    //调用渲染分页
                renderPage(res.allNum)
            }
        });
    }
    // 定义渲染分页的方法
    function renderPage(allNum) {
        console.log(allNum);
        // 调用 laypage.render()方法来渲染分页的结构
        laypage.render({
            elem: 'pageBox', //分页容器的id
            count: allNum, //总数据条
            limit: q.pagesize, //每页显示几条数据
            curr: q.start, //设置默认被选中的分页
            layout: ['count', 'limit', 'prev', 'page', 'next'],
            limits: [2, 4, 8, 10],
            // 触发jump的方式有两种
            // 1.点击页码 触发回调
            // 2.调用laypage.render就会触发回调
            jump: function(obj, first) {
                q.start = obj.curr
                q.pagesize = obj.limit
                    // 通过first的值判断哪种方式触发的jump 解决死循环
                if (!first) {
                    initTable()
                }
            }
        })

    }
    //通过代理的形式，为删除按钮点击绑定事件
    $('tbody').on('click', '.btn-deletes', function() {
        var len = $('.btn-deletes').length
        console.log('ok');
        var data = {
            act: 'del',
            id: $(this).attr('data-id'),
        }
        console.log(this);
        layer.confirm('确认删除?', { icon: 3, title: '提示' }, function(index) {
            $.ajax({
                type: "get",
                url: 'http://122.51.249.55:8083/api/advpos',
                data,
                success: function(res) {
                    if (res.code !== 200) {
                        return layer.msg('删除失败！')
                    }
                    layer.msg('删除成功')

                    if (len === 1) {
                        // 判断页码值是否为1，如果是1，则只能是1
                        q.pagenum = q.pagenum === 1 ? 1 : q.pagenum - 1;


                    }
                    initTable()
                }
            })
            layer.close(index)
        })
    })

    //为添加按钮点击绑定事件


    // tab栏切换
    $('#one').on('click', function() {
        $('#gsgs').show()
        $('#gsywly').hide()
    });
    $('#char').on('click', function() {
        $('#gsywly').show()
        $('#gsgs').hide()
    });
    $('.add_link').on('click', function() {
        $(this).addClass('link_list').siblings().removeClass('link_list');
    })

    // 添加类别按钮绑定点击事件
    var indexEdit = null
    var id
    $('body').on('click', '#btnAddCate', function() {
            layer.open({
                    type: 1,
                    area: ['500px', '400px'],
                    title: '修改广告位信息',
                    content: $('#tpl-cate').html()
                })
                // 渲染表单数据        
            id = $(this).attr('data-id')
            var advposname = $(this).attr('data-advposname')
            var advposdesc = $(this).attr('data-advposdesc')
            var advpossize = $(this).attr('data-advpossize')
            form.val('form-addc', {
                id: id,
                advposname: advposname,
                advposdesc: advposdesc,
                advpossize: advpossize
            })
        })
        // 发起请求修改
    $('body').on('submit', '#form-addc', function(e) {
        e.preventDefault()
        let data = $(this).serialize();
        console.log(this);

        $.ajax({
            type: 'post',
            url: 'http://122.51.249.55:8083/api/advpos',
            data: data,
            success: function(res) {
                if (res.code !== 200) {
                    return layer.msg('更新广告位数据失败')
                }
                layer.msg('更新广告位数据成功')
                    // layer.close(indexEdit)
                    // 直接关闭弹窗
                layui.layer.closeAll()
                initTable()
            }
        })
    })




    $('#form-add').on('submit', function(e) {
        var qc = {
            act: 'add',
            advposname: $('#form-add [name=advposname]').val(),
            advposdesc: $('#form-add [name=advposdesc]').val(),
            advpossize: $('#form-add [name=advpossize]').val(),
        }
        console.log(qc);
        e.preventDefault()
        $.ajax({
            method: 'post',
            url: 'http://122.51.249.55:8083/api/advpos',
            data: qc,
            success: function(res) {
                if (res.code !== 200) {
                    return layer.msg('添加广告位接口失败')
                }
                initTable()
                layer.msg('添加广告位接口成功')
            }
        })
    })
})