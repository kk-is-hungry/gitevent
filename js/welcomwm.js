(function() {
    // 后台首页数据接口
    getManagementData()

    // 后台请求数据
    function getManagementData() {
        $.ajax({
            type: "GET",
            url: "http://122.51.249.55:8083/api/index",
            success: function(res) {
                if (res.code !== 200) return console.log('获取后台首页数据失败');

                renderFirstRow(res.data)
                    // getArticleData(res.data)
                dataHandler(res.data.getMenuArticle)
                monthHandler(res.data.getYearData)

            }
        });

    }
    // 第一行从后台获取数据并填充
    function renderFirstRow(data) {
        $('#adminUser').html(data.getAllAdminNum);
        $('#regUser').html(data.getAllUserNum);
        $('#articleNum').html(data.geAllArtilceNum);
        $('#advPos').html(data.getAllAdvPos);
        $('#advImg').html(data.getAllAdvImg);
    }

    // 数据处理
    function dataHandler(data) {
        var arrcate = [];
        var arrnum = [];
        data.map((item, index) => {
            return arrcate.push(item.catename)

        })
        data.map((item, index) => {
                return arrnum.push(item.num);
            })
            // console.log(arrcate);
            // console.log(arrnum);
        getArticleData(arrcate, arrnum)
    }
    // 文章发布数量及来源分布
    (function() {
        // 1. 实例化对象
        var myChart = echarts.init(document.querySelector(".pie"));
        // 2. 指定配置项和数据
        var option = {

            visualMap: {
                top: 0,
                right: 0,
                pieces: [{
                        lt: 10,
                        color: '#d48265'
                    },
                    {
                        gt: 10,
                        lte: 50,
                        color: '#91c7ae'
                    }, {
                        gt: 100,
                        color: '#ca8622'
                    }
                ],
                // 翻转图例的顺序
                inverse: true
            },
            series: [

                // 里层饼图数据
                {
                    name: '访问来源',
                    type: 'pie',
                    selectedMode: 'single',
                    radius: [0, '30%'],
                    label: {
                        position: 'inner',
                        fontSize: 14,
                    },
                    labelLine: {
                        show: false
                    },
                    data: [
                        { value: 180, name: 'pc端', selected: true, itemStyle: { normal: { color: '#c23531' } } },
                        { value: 300, name: '安卓端', itemStyle: { normal: { color: '#2f4554' } } },
                        { value: 679, name: 'IOS端', itemStyle: { normal: { color: '#61a0a8' } } }
                    ],

                },
                // 外层环形数据
                {
                    name: '访问来源',
                    type: 'pie',
                    radius: ['45%', '60%'],
                    labelLine: {
                        length: 20,
                        length2: 10,
                        lineStyle: {
                            //  color:['#d48265','#91c7ae','#749f83','#ca8622'],
                        }
                    },

                    data: [
                        { value: 650, name: '<10', itemStyle: { normal: { color: '#d48265' } } },
                        { value: 450, name: '10-50', itemStyle: { normal: { color: '#91c7ae' } } },
                        { value: 510, name: '50-100', itemStyle: { normal: { color: '#749f83' } } },
                        { value: 200, name: '>100', itemStyle: { normal: { color: '#ca8622' } } }

                    ],

                }
            ]
        };
        myChart.setOption(option);
    })();

    //各分类文章数据分析-柱形图
    function getArticleData(arrcate, arrnum) {
        // console.log(managementData);
        var myChart = echarts.init(document.querySelector(".bar"));
        var option = {
            color: ['#c23531'],
            xAxis: {
                type: 'category',
                data: arrcate,
                axisTick: {
                    // true意思：图形在刻度中间
                    // false意思：图形在刻度之间
                    alignWithLabel: true
                }
            },
            yAxis: {
                type: 'value',
                axisLine: {
                    show: true,
                },
                // 网格颜色
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: ['#cccccc'],
                        width: 1,
                        type: 'solid'
                    }
                }
            },
            series: [{
                data: arrnum,
                type: 'bar'
            }]
        };
        myChart.setOption(option);
    }

    // 12月数据处理
    /**
        *  {
       catename: "455"
       current: [1, 0, 0, 0, 0, 0, 0, 5, 11, 13, 16, 0]
       id: 3
       num: 29
       }
        */
    function monthHandler(data) {
        var arryList = [];
        var arrcate = [];
        data.map((item, index) => {
            return arrcate.push(item.catename)

        })
        for (var i = 0; i < data.length; i++) {
            /**
             *{
            name: '邮件营销',
            type: 'line',
            stack: '总量',
            areaStyle: {},
            emphasis: {
                focus: 'series'
            },
            data: [120, 132, 101, 134, 90, 230, 210]
        },
             */
            // 将数据库的数据转换成echarts数据结构
            var obj = convertMonthObj(data[i]);
            // arryList[i] = obj;
            arryList.push(obj)
            getArticleMonth(arrcate, arryList)
        }
    }

    function convertMonthObj(param) {
        var obj = {};
        obj.name = param.catename;
        obj.type = 'line';
        obj.stack = '总量';
        obj.areaStyle = {};
        // obj.emphasis = {
        //     focus: 'series'
        // };
        obj.data = param.current;
        return obj;
    }
    //近12年各月文章数量发布数分析-折线图
    function getArticleMonth(arrcate, arryList) {
        var myChart = echarts.init(document.querySelector(".line"));
        var option = {
            color: ['#d06360', '#576875', '#88b7bd', '#de9f88', '#a2d0ba', '#8db099', '#d59f51', '#c3aba4', '#acacaf'],

            tooltip: {

                trigger: 'axis',

                axisPointer: {

                    type: 'cross',

                    label: {

                        backgroundColor: '# 6 a7985 '
                    }
                }
            },
            legend: {
                data: arrcate
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [{
                type: 'category',
                boundaryGap: false,
                data: ['10月份', '11月份', '12月份', '1月份', '2月份', '3月份', '4月份',
                    '5月份', '6月份', '7月份', '8月份', '9月份'
                ]
            }],
            yAxis: [{
                type: 'value',
                axisLine: {
                    show: true,
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: ['#cccccc'],
                        width: 1,
                        type: 'solid'
                    }
                }
            }],
            series: arryList
        };
        myChart.setOption(option);
    }
})()