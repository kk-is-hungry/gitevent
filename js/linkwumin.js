(function() {
    var layer = layui.layer;
    var form = layui.form;
    var laypage = layui.laypage;

    // 点击链接列表，添加链接列表的显示与隐藏
    $('#link_list').on('click', function() {
        $('#upload_link').hide()
        $('#main_link').show()
        $(this).addClass('orange');
        $(this).parent().siblings().children().removeClass('orange');
    })
    $('#add_link').on('click', function() {
        $('#main_link').hide()
        $('#upload_link').show()
        $(this).addClass('orange');
        $(this).parent().siblings().children().removeClass('orange');
    })


    // 定义一个查询的参数对象，将来请求数据的时候，
    // 需要将请求参数对象提交到服务器
    var q = {
            act: 'getlist', // 页码值，默认请求第一页的数据
            type: 1, // 0所有，1文字，2图片
            start: 1, // 开始页数
            pagesize: 10 // 每页条数
        }
        // 先声明后调用
        // 文字链接渲染
    getFriendLink()
        // 图片链接渲染
    getImgLink()
        // 获取文字链接，初始页面
    var tableDataWord;

    function getFriendLink() {
        $.ajax({
            method: "GET",
            url: "http://122.51.249.55:8083/api/link",
            data: q,
            success: function(res) {

                if (res.code !== 200) return console.log('获取友情链接失败');
                tableDataWord = res.data;
                var htmlStr = template('tpl_table_word',
                    res)
                $('.wordLink tbody').html(htmlStr)
                    //  调用分页;两个页面，设置了2个参数
                renderPage(res.allNum, 'page_friendLinks')
            }
        });
    }

    var tableDataImg;
    // 获取图片链接，初始页面
    function getImgLink() {
        q.type = 2;
        $.ajax({
            method: "GET",
            url: "http://122.51.249.55:8083/api/link",
            data: q,
            success: function(res) {

                if (res.code !== 200) return console.log('获取友情链接失败');
                tableDataImg = res.data;
                var htmlStr = template('tpl_table_img',
                    res)
                $('.imgLink tbody').html(htmlStr)
                    // 调用分页;两个页面，设置了2个参数
                renderPage(res.allNum, 'page_wordLinks')
            }
        });
    }

    // 封装分页的函数
    function renderPage(allnum, link) {
        laypage.render({
            elem: link, //注意，这里的 test1 是 ID，不用加 # 号
            count: allnum, //数据总数，从服务端得到
            limit: q.pagesize,
            curr: q.start, // 设置默认被选中的分页
            layout: ['count', 'limit', 'page'],
            limits: [1, 10, 15, 20],
            jump: function(obj, first) {
                // 可以通过 first 的值，来判断是通过哪种方式，触发的 jump 回调
                // 如果 first 的值为 true，证明是方式2触发的
                // 否则就是方式1触发的
                // 把最新的页码值，赋值到 q 这个查询参数对象中
                q.start = obj.curr
                    // 把最新的条目数，赋值到 q 这个查询参数对象的 pagesize 属性中
                q.pagesize = obj.limit
                    // 根据最新的 q 获取对应的数据列表，并渲染表格
                    // initTable()
                if (!first) {
                    getFriendLink()
                }
            }
        });
    }

    // 点击编辑按钮绑定点击事件-文字链接
    var indexEdit = null;
    var targetWord;
    // var objEdit = {};
    $('tbody').on('click', '.linkEdit', function() {
        indexEdit = layer.open({
            type: 1,
            title: '修改链接信息',
            area: ['500px', '300px'],
            content: $('#edit_links').html() //这里content是一个普通的String
        });

        // 获取点击的id,数据回显
        var id = $(this).attr('data-id');
        // var target;
        if (tableDataWord != null) {
            for (var i = 0; i < tableDataWord.length; i++) {
                if (tableDataWord[i].id == id) {
                    targetWord = tableDataWord[i];
                    // console.log(target);
                    break;
                }
            }
        }
        form.val('form-edit', targetWord);
    })

    // 修改链接的提交事件
    $('body').on('submit', '#form-edit', function(e) {
        e.preventDefault();
        targetWord.url = $('#form-edit [name = "url"]').val();
        targetWord.des = $('#form-edit [name = "des"]').val();
        targetWord.title = $('#form-edit [name = "title"]').val();
        targetWord.act = "edit";
        //修改
        getEditLinks(targetWord, 'form-edit')
            // 关闭弹出层

    })

    //修改弹出框取消按钮事件
    $('body').on('click', '#cancel_btn', function() {
        layer.close(indexEdit);
    })

    // 后台修改的接口请求


    function getEditLinks(objEdit, myform) {
        $.ajax({
            type: "POST",
            url: "http://122.51.249.55:8083/api/link",
            data: objEdit,
            success: function(res) {
                if (res.code !== 200) return console.log('获取此id的信息失败');
                // form.val('form-edit', res.data)
                // form.val(myform, res.data)
                layer.msg('修改文字链接成功!');
                layer.close(indexEdit);
                //重新刷新表格
                q.type = 1;
                getFriendLink();
            }
        });
    }

    // 删除模块；点击删除按钮
    $('tbody').on('click', '.linkDelete', function() {
        // 获取删除按钮的个数
        var len = $('.linkDelete').length
        console.log(len);
        var id = $(this).attr('data-id')
        layer.confirm('确定删除该链接吗?', { icon: 3, title: '提示' }, function(index) {
            //do something
            $.ajax({
                type: "GET",
                url: "http://122.51.249.55:8083/api/link",
                data: {
                    id: id,
                    act: 'del'
                },
                success: function(res) {
                    if (res.code !== 200) return layer.msg('删除失败')
                    layer.msg('删除成功')
                    if (len === 1) {
                        // 如果 len 的值等于1，证明删除完毕之后，页面上就没有任何数据了
                        // 页码值最小必须是 1
                        q.start = q.start === 1 ? 1 : q.start - 1
                    }
                    q.type = 1;
                    getFriendLink();
                }
            });
            layer.close(index);
        });
    })


    // 图片链接模块

    // 点击编辑绑定事件-图片链接
    var indexImg = null;
    // var objEdit = {};
    var targetImg;
    $('tbody').on('click', '.linkEditAmg', function() {
        indexImg = layer.open({
            type: 1,
            title: '修改链接信息',
            area: ['500px', '400px'],
            content: $('#img_links').html() //这里content是一个普通的String
        });
        // 获取点击的id
        var id = $(this).attr('data-id');
        if (tableDataImg != null) {
            for (var i = 0; i < tableDataImg.length; i++) {
                if (tableDataImg[i].id == id) {
                    targetImg = tableDataImg[i];
                    break;
                }
            }
        }
        /**
         * des: "描述"
id: 288
img: "C:fakepathSnipaste_2021-09-04_13-39-12.png"
src: "C:fakepathSnipaste_2021-09-04_13-39-12.png"
title: null
type: "2"
url: "地址"
         */
        form.val('form-img', targetImg);
        $('.photo img').attr('src', "http://122.51.249.55:8083" +
            targetImg.src);
    })

    //修改弹出框取消按钮事件-图片
    $('body').on('click', '#cancel_btn_img', function() {
        layer.close(indexImg);
    })

    // 上传文件；模拟点击行为
    $('body').on('click', '.uploadBtn', function() {
            $('.hidbtnUpload').click()
        })
        //监听change事件，用户是否选择了文件
    $('body').on('change', '.hidbtnUpload', function(e) {
            var files = $('.hidbtnUpload')[0].files
            if (files.length === 0) {
                return
            }
            // console.log(files);
            // jq对象转为dom对象
            var formData = new FormData();
            formData.append("img", files[0]); //上传一个files对象
            $.ajax({ //jQuery方法，此处可以换成其它请求方式
                url: 'http://122.51.249.55:8083/api/upload',
                dataType: "json",
                type: "post",
                data: formData,
                processData: false, //不去处理发送的数据
                contentType: false, //不去设置Content-Type请求头
                error: function(res) {

                },
                success: function(res) {
                    //回显上传的图片
                    $('.photo img').attr('src', "http://122.51.249.55:8083" +
                        res.data[0].path);
                    //存储上传路径！
                    $('#hidUploadIput').val(res.data[0].path);
                }

            })
        })
        // 表单提交事件
    $('body').on('submit', '#form-img', function(e) {
        e.preventDefault();
        targetImg.url = $('#form-img [name = "url"]').val();
        targetImg.des = $('#form-img [name = "des"]').val();
        targetImg.img = $('#hidUploadIput').val();
        targetImg.act = "edit";
        // formData.append($(this).serialize())
        $.ajax({
            type: "post",
            url: "http://122.51.249.55:8083/api/link",
            data: targetImg,
            success: function(res) {
                if (res.code != 200) return layer.msg('修改图片信息失败')
                layer.msg('修改图片信息成功');
                layer.close(indexImg);
                q.type = 2;
                getImgLink();
            }
        });
    })

    // 图片链接的删除
    $('tbody').on('click', '.linkDeleteAmg', function() {
        // 获取删除按钮的个数
        var len = $('.linkDeleteAmg').length
        var id = $(this).attr('data-id')
        layer.confirm('确定删除该链接吗?', { icon: 3, title: '提示' }, function(index) {
            //do something
            $.ajax({
                type: "GET",
                url: "http://122.51.249.55:8083/api/link",
                data: {
                    id: id,
                    act: 'del'
                },
                success: function(res) {
                    if (res.code !== 200) return layer.msg('删除失败')
                    layer.msg('删除成功')
                    if (len === 1) {
                        // 如果 len 的值等于1，证明删除完毕之后，页面上就没有任何数据了
                        // 页码值最小必须是 1
                        q.start = q.start === 1 ? 1 : q.start - 1
                    }
                    q.type = 2;
                    getImgLink();
                }
            });
            layer.close(index);
        });
    })


    // 上传文件；模拟点击行为
    $('body').on('click', '.uploadBtnAdd', function() {
            $('.hidAddIput').click()
        })
        //监听change事件，用户是否选择了文件
    $('body').on('change', '.hidAddIput', function(e) {
        var files = $('.hidAddIput')[0].files
        if (files.length === 0) {
            return
        }
        // console.log(files);
        // jq对象转为dom对象
        var formData = new FormData();
        formData.append("img", files[0]); //上传一个files对象
        $.ajax({ //jQuery方法，此处可以换成其它请求方式
            url: 'http://122.51.249.55:8083/api/upload',
            dataType: "json",
            type: "post",
            data: formData,
            processData: false, //不去处理发送的数据
            contentType: false, //不去设置Content-Type请求头
            error: function(res) {

            },
            success: function(res) {
                $('#hid_photoArea').show()
                    //回显上传的图片
                $('.photoAdd img').attr('src', "http://122.51.249.55:8083" + res.data[0].path);
                //存储上传路径！
                $('#hidAddIput').val(res.data[0].path);
            }

        })
    })


    // $('#choose_type').on('change', function() {
    //     console.log(12312312);
    //     var mytype = $('#add_link_form [name="type"]').val();
    //     if (mytype == 1) {
    //         $('#onlyword_title').show();
    //         $('#onlyImg_photo').hide()
    //     } else if (mytype == 2) {
    //         $('#onlyword_title').hide();
    //         $('#onlyImg_photo').show();
    //     }
    // })

    form.on('select(demo)', function(data) {
        console.log(123123);
        var mytype = $('#add_link_form [name="type"]').val();
        if (mytype == 1) {
            // 清空表单的值
            $('#add_link_form')[0].reset();
            // 被清空重新赋值
            $('#add_link_form [name="type"]').val(1);
            $('#onlyword_title').show();
            $('#onlyImg_photo').hide();
            $('#hid_photoArea').hide();
        } else if (mytype == 2) {
            $('#add_link_form')[0].reset();
            $('#add_link_form [name="type"]').val(2);
            $('#onlyword_title').hide();
            $('#onlyImg_photo').show();
        }
    });



    // 添加链接部分
    $('#add_link_form').on('submit', function(e) {
        e.preventDefault()
        var data = $(this).serialize() + '&act=add'
        var mytype = $('#add_link_form [name="type"]').val();
        console.log(mytype);
        $.ajax({
            type: "POST",
            url: "http://122.51.249.55:8083/api/link",
            data: data,
            success: function(res) {
                if (res.code !== 200) return layer.msg('添加失败');
                layer.msg('添加成功');
                if (mytype == 1) {
                    q.type = 1;
                    getFriendLink();
                } else if (mytype == 2) {
                    q.type = 2;
                    getImgLink();
                }

            }
        });
    })
})()