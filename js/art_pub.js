var layedit = layui.layedit;

$(function() {
    // $('#wen_title').val(localStorage.getItem('title'));
    // // $('#select-1').val(localStorage.getItem('catename'));
    // $('#wen_author').val(localStorage.getItem('author'));
    // $('#wen_click').val(localStorage.getItem('num'));
    // $('#keywords').val(localStorage.getItem('keywords'));
    // $('#description').val(localStorage.getItem('description'));


    // 下拉选择框
    layui.use('form', function() {
        var form = layui.form;
        //监听提交
        form.on('submit(formDemo)', function(data) {
            layer.msg(JSON.stringify(data.field));
            return false;
        });
    });
    layui.use('layedit', function() {
        var layedit = layui.layedit;
        layedit.build('fotext'); //建立编辑器
    });

    //   获取文章
    getArticle()
        //   获取分类
    getArticleMenu()
        //   获取标签
    getArticleTags()
        //修改文章

    var form = layui.form
    var id;

    //   获取文章接口
    function getArticle() {
        id = localStorage.getItem("articleid")
        console.log(id);
        $.ajax({
            method: "GET",
            url: "http://122.51.249.55:8083/api/article",
            data: {
                act: "find",
                id: id,
            },
            success: function(res) {
                if (res.code !== 200) return layui.msg(res.msg)
                    //   填充数据到第一页表单
                form.val('test1', res.data)
                console.log(res);
                $("#keywords").val(res.data.keywords)
                $("#description").val(res.data.description)

                // 缩略图渲染
                if (res.data.picimg.length == 0) {
                    $("#imgBox").hide()
                }
                $("#dogimg").attr("src", res.data.picimg[0].url)
                $("#imgBox").show()
                    // 富文本赋值
                layui.use(['layedit'], function() {
                    var layedit = layui.layedit;
                    var index = layedit.build('fotext', { height: 480 }); //建立编辑器
                    layedit.setContent(index, res.data.content);
                })

            }
        })
    }

    //   获取分类接口
    function getArticleMenu() {
        $.ajax({
            method: "GET",
            url: "http://122.51.249.55:8083/api/menu",
            data: {
                act: "getlist",
                pagesize: 200,
            },
            success: function(res) {
                // console.log(12313);
                if (res.code !== 200) return layui.msg(res.msg)
                var htmlStr = template("tpl-select", res)
                $("#select-1").html(htmlStr)
                form.render('select')
            }
        })
    }
    //   获取复选框

    function getArticleTags() {
        $.ajax({
            method: "GET",
            url: "http://122.51.249.55:8083/api/tag",
            data: {
                act: "getlist",
                pagesize: 200,
            },
            success: function(res) {
                if (res.code !== 200) return layui.msg(res.msg)
                var htmlStr = template("tpl-tags", res)
                $("#tags").html(htmlStr)
                form.render('checkbox')
            }
        })
    }

    // 为了获取分类id,套个form监听事件
    form.on('select(category)', function(data) {
        // 获取分类id
        var cateid = data.value
            // console.log(cateid);
        id = localStorage.getItem("articleid")
            //修改文章
        form.on('submit(formDemo)', function() {
            $.ajax({
                    method: "POST",
                    url: "http://122.51.249.55:8083/api/article",
                    data: {
                        act: "edit",
                        id: id,
                        title: $(".layui-form [name=title]").val(),
                        cateid: cateid,
                        author: $(".layui-form [name=author]").val(),
                    },
                    success: function(res) {
                        if (res.code !== 200) return layer.msg(res.msg)
                        layer.msg(res.msg)
                        location.href = "../art_list.html"
                    }
                })
                // 这行代码勿动!!!!
            return false
        })
    });

})